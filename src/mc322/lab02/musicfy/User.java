package mc322.lab02.musicfy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.TreeSet;

public class User {

    // Static Properties
    private static String formatString = "dd/MM/yyyy";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(User.formatString);

    // Properties
    private String name;
    private String cpf;
    private LocalDate birthDate;
    private Gender gender;
    private TreeSet<Playlist> playlists;
    private Subscription subscription;

    // Enumerators
    public enum Gender {
        MALE("Male"),
        FEMALE("Female"),
        OTHER("Other");

        String label;

        Gender(String label) {
            this.label = label;
        }
    }

    public enum Subscription {
        STANDARD("Standard"),
        PREMIUM("Premium");

        String label;

        Subscription(String label) {
            this.label = label;
        }
    }

    // Initializers
    public User(String name, String cpf, String birthDateString, Gender gender, Subscription subscription) {
        Comparator<Playlist> comparator = new Comparator<Playlist>() {
            @Override
            public int compare(Playlist s1, Playlist s2) {
                return s1.getName().compareTo(s2.getName());
            }
        };

        this.setName(name);
        this.setCpf(cpf);
        this.setBirthDateFromString(birthDateString);
        this.setGender(gender);
        this.playlists = new TreeSet<Playlist>(comparator);
        this.dangerousSetSubscription(subscription);
    }

    // Methods
    public void showInformation() {
        System.out.println("User name: " + this.getName());
        System.out.println("CPF: " + this.getCpf());
        System.out.println("Birth: " + this.getBirthDateAsString());
        System.out.println("Gender: " + this.getGender().label);
        System.out.println("Subscription: " + this.getSubscription().label);
    }

    public void showPlaylists() {
        System.out.println("User name: " + this.getName());
        System.out.println("Number of playlists: " + this.getPlaylists().size());
        for (Playlist playlist: this.getPlaylists()) {
            playlist.showInformation("  ");
        }
    }

    public void setSubscription(Subscription subscription) {
        switch (subscription) {
            case PREMIUM:
                this.dangerousSetSubscription(subscription);
                break;
            default:
                System.out.println("Attention: downgrading subscription may erase playlists that have more songs than allowed. It also may erase playlists if user has more than the allowed for new subscription. To do that, use method dangerousSetSubscription()");
                break;
        }
    }

    public void dangerousSetSubscription(Subscription subscription) {
        this.subscription = subscription;

        // Remove playlists with more songs than allowed
        for (Playlist playlist: this.getPlaylists()) {
            if (playlist.getNumberOfSongs() > playlist.getPlaylistMaxSize()) {
                this.playlists.remove(playlist);
            }
        }

        // Remove playlists until the number of them is allowed
        while (this.getPlaylists().size() > this.getMaxPlaylistNumber()) {
            this.playlists.remove(this.getPlaylists().last());
        }
    }

    public void transferPlaylistToUser(String playlistName, User user) {
        Playlist toTransfer = getPlaylistByName(playlistName);
        if (toTransfer != null) {
            user.addPlaylist(toTransfer);
            this.playlists.remove(toTransfer);
        }
    }

    public void newPlaylist(String playlistName) {
        Playlist playlist = new Playlist(playlistName, this);
        this.addPlaylist(playlist);
    }

    private void addPlaylist(Playlist playlist) {
        if (this.getPlaylists().size() < this.getMaxPlaylistNumber()) {
            this.playlists.add(playlist);
        } else {
            System.out.println("Error: trying to add new playlist '" + playlist.getName() + "' to user with maximum number of playlists");
        }
    }

    public void removePlaylistByName(String playlistName) {
        Playlist toRemove = this.getPlaylistByName(playlistName);
        if (toRemove != null) {
            this.playlists.remove(toRemove);
        }
    }

    public Playlist getPlaylistByName(String playlistName) {
        Playlist toReturn = null;
        for (Playlist playlist: this.getPlaylists()) {
            if (playlist.getName() == playlistName) {
                toReturn = playlist;
                break;
            }
        }

        if (toReturn == null) {
            System.out.println("Error: no playlist named '" + playlistName + "'");
        }

        return toReturn;
    }

    public TreeSet<Playlist> getPlaylists() {
        return this.playlists;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setCpf(String cpf) {
        if (isValidCpf(cpf)) {
            this.cpf = cpf;
        }
    }

    public String getCpf() {
        return this.cpf;
    }

    public void setBirthDateFromString(String birthDateString) {
        try {
            LocalDate birthDate = LocalDate.parse(birthDateString, formatter);
            this.setBirthDate(birthDate);
        } catch (Exception e) {
            System.out.println("Error: invalid date string: '" + birthDateString + "' Use format " + User.formatString);
        }
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDateAsString() {
        String birthDateString = this.birthDate.format(User.formatter);
        return birthDateString;
    }

    public LocalDate getBirthDate() {
        return this.birthDate;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getGender() {
        return this.gender;
    }

    public Subscription getSubscription() {
        return this.subscription;
    }

    private int getMaxPlaylistNumber() {
        int maxNumber;
        switch (this.getSubscription()) {
            case PREMIUM:
                maxNumber = 10;
                break;
            default:
                maxNumber = 3;
                break;
        }

        return maxNumber;
    }

    // Static Methods
    private static boolean isValidCpf(String cpf) {
        boolean valid = true;

        if (cpf.length() == 14) {
            for (int i = 0; i < 14; i++) {
                if (i == 3 || i == 7) {
                    if (cpf.charAt(i) != '.') {
                        valid = false;
                    }

                } else if (i == 11) {
                    if (cpf.charAt(i) != '-') {
                        valid = false;
                    }

                } else {
                    if (!Character.isDigit(cpf.charAt(i))) {
                        valid = false;
                    }
                }
            }

        } else {
            valid = false;
        }

        if (valid) {
            return true;

        } else {
            System.out.println("Error: invalid cpf: '" + cpf + "' Use format XXX.XXX.XXX-XX");
            return false;
        }
    }
}
