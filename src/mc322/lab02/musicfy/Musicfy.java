package mc322.lab02.musicfy;

public class Musicfy {

    public static void main(String[] args) {

        User user1 = new User("Marcos Paulo", "777.777.777-77", "29/05/1971", User.Gender.MALE, User.Subscription.STANDARD);
        User user2 = new User("Cookiezi", "111.111.111-11", "13/12/2000", User.Gender.FEMALE, User.Subscription.PREMIUM);

        Song song1 = new Song("Seven Nation Army", "The White Stripes", "PT2M12S", Song.MusicGenre.ROCK);
        Song song2 = new Song("Crazy Train", "Ozzy Osbourne", "PT3M41S", Song.MusicGenre.ROCK);
        Song song3 = new Song("Feels", "Calvin Harris", "PT2M51S", Song.MusicGenre.POP);
        Song song4 = new Song("Roar", "Katy Perry", "PT3M11S", Song.MusicGenre.POP);
        Song song5 = new Song("Anima", "Xi", "PT4M33S", Song.MusicGenre.HARDCORE);
        Song song6 = new Song("Freedom Dive", "Xi", "PT7M11S", Song.MusicGenre.HARDCORE);
        Song song7 = new Song("Teo", "Omoi", "PT5M19S", Song.MusicGenre.HARDCORE);
        Song song8 = new Song("Sleepwalking", "Bring Me The Horizon", "PT4M52S", Song.MusicGenre.METALCORE);

        user1.newPlaylist("Awesome Rock Songs");
        Playlist rockPlaylist = user1.getPlaylistByName("Awesome Rock Songs");
        rockPlaylist.addSong(song1);
        rockPlaylist.addSong(song2);

        user1.newPlaylist("Osu Memories");
        Playlist osuPlaylist = user1.getPlaylistByName("Osu Memories");
        osuPlaylist.addSong(song5);
        osuPlaylist.addSong(song6);
        osuPlaylist.addSong(song7);

        user2.newPlaylist("Poppin off");
        Playlist popPlaylist = user2.getPlaylistByName("Poppin off");
        popPlaylist.addSong(song3);
        popPlaylist.addSong(song4);

        user2.newPlaylist("Best of Metalcore");
        Playlist metalcorePlaylist = user2.getPlaylistByName("Best of Metalcore");
        metalcorePlaylist.addSong(song8);

        user1.showPlaylists();
        System.out.println();
        user2.showInformation();

        System.out.println();
        System.out.println("Play osu: " + osuPlaylist.play().getName());
        System.out.println("Play osu: " + osuPlaylist.play().getName());
        System.out.println("Play osu shuffle: " + osuPlaylist.play(true).getName());
    }
}
