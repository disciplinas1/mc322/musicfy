package mc322.lab02.musicfy;

import java.time.Duration;

public class Song {

    // Static Properties
    private static String durationStringFormat = "mm:ss";

    // Properties
    private String name;
    private String artist;
    private Duration duration;
    private MusicGenre genre;

    // Enumerators
    public enum MusicGenre {
        ROCK("Rock"),
        POP("Pop"),
        ELETRONIC("Eletronic"),
        HARDCORE("Hardcore"),
        METALCORE("Metalcore");

        String label;

        MusicGenre(String label) {
            this.label = label;
        }
    }

    // Initializers
    public Song(String name, String artist, String duration, MusicGenre genre) {
        this.setName(name);
        this.setArtist(artist);
        this.setDurationFromString(duration);
        this.setMusicGenre(genre);
    }

    // Methods
    public void showInformation() {
        System.out.println("Song name: " + this.getName());
        System.out.println("Artist: " + this.getArtist());
        System.out.println("Duration: " + this.getDurationAsString());
        System.out.println("Genre: " + this.getMusicGenre().label);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtist() {
        return this.artist;
    }

    public void setDurationFromString(String durationString) {
        try {
            Duration duration = Duration.parse(durationString);
            this.setDuration(duration);
        } catch (Exception e) {
            System.out.println("Error: invalid duration string: '" + durationString + "' Use format " + Song.durationStringFormat);
        }
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public String getDurationAsString() {
        Duration duration = this.getDuration();
        return Util.durationToString(duration);
    }

    public Duration getDuration() {
        return this.duration;
    }

    public void setMusicGenre(MusicGenre genre) {
        this.genre = genre;
    }

    public MusicGenre getMusicGenre() {
        return this.genre;
    }
}
