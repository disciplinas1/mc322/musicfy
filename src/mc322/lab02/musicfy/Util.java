package mc322.lab02.musicfy;

import java.time.Duration;
import java.util.Random;
import java.util.Set;

public class Util {

    public static <T> T pickRandomElementFromSet(Set<T> set) {
        int randomPick = new Random().nextInt(set.size());
        T returnObj = null;
        int i = 0;
        for (T obj: set) {
            if (i == randomPick) {
                returnObj = obj;
            }

            i++;
        }

        return returnObj;
    }

    public static String durationToString(Duration duration) {
        int seconds = duration.toSecondsPart();
        int minutes = duration.toMinutesPart();
        int hours = duration.toHoursPart();

        String durationString;
        if (hours == 0) {
            durationString = String.format("%d:%02d", minutes, seconds);
        } else if (hours > 0) {
            durationString = String.format("%d:%02d:%02d", hours, minutes, seconds);
        } else {
            System.out.println("Error: negative duration not supported, returning empty string");
            durationString = "";
        }

        return durationString;
    }
}
