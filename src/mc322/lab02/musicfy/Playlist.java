package mc322.lab02.musicfy;

import java.time.Duration;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.HashSet;

public class Playlist {

    // Properties
    private String name;
    private User user;
    private TreeSet<Song> songs;
    private Song lastPlayedSong;

    // Initializers
    public Playlist(String name, User user) {
        Comparator<Song> comparator = new Comparator<Song>() {
            @Override
            public int compare(Song s1, Song s2) {
                return s1.getName().compareTo(s2.getName());
            }
        };

        this.name = name;
        this.user = user;
        this.songs = new TreeSet<Song>(comparator);
        this.lastPlayedSong = null;
    }

    // Methods
    public void showInformation(String identationString) {
        System.out.println(identationString + "Playlist name: " + this.getName());
        System.out.println(identationString + "Number of songs: " + this.getNumberOfSongs());
        System.out.println(identationString + "Playlist duration: " + this.getFullPlaylistDurationAsString());
        System.out.println(identationString + "Mean song duration: " + this.getMeanPlaylistDurationAsString());
        System.out.println(identationString + "Songs:");
        this.showSongs(identationString + "  ");
    }

    public void showSongs(String identationString) {
        for (Song song: this.songs) {
            System.out.println(identationString + song.getName() + " - " + song.getArtist());
        }
    }

    public Song play() {
        return this.play(false);
    }

    public Song play(boolean shuffle) {
        if (this.isEmpty()) {
            System.out.println("Error: trying to play from empty playlist: " + this.getName());
            return null;
        }

        Song songToPlay;
        if (!shuffle) {
            if (this.lastPlayedSong == null || this.lastPlayedSong == this.getSongs().last()) {
                songToPlay = this.getSongs().first();
            } else {
                songToPlay = this.getSongs().higher(this.lastPlayedSong);
            }
        } else {
            if (this.getSongs().size() > 1) {
                // Pick random song different from lastPlayedSong
                do {
                    songToPlay = Util.pickRandomElementFromSet(this.getSongs());
                } while (songToPlay == this.lastPlayedSong);
            } else {
                songToPlay = this.getSongs().first();
            }
        }

        this.lastPlayedSong = songToPlay;
        return songToPlay;
    }

    public String getArtistWithMostAppearances() {
        if (this.isEmpty()) {
            System.out.println("Error: trying to get artist with most appearances from empty playlist: " + this.getName());
            return null;
        }

        int maxAppearances = 0;
        String mostAppearancesArtist = "";
        HashSet<String> checkedArtists = new HashSet<String>();
        for (Song song: this.songs) {
            String artist = song.getArtist();
            if (!checkedArtists.contains(artist)) {
                checkedArtists.add(artist);
                int count = this.countArtistAppearances(artist);
                if (count > maxAppearances) {
                    maxAppearances = count;
                    mostAppearancesArtist = artist;
                }
            }
        }

        return mostAppearancesArtist;
    }

    public int countArtistAppearances(String artist) {
        int count = 0;
        for (Song song: this.songs) {
            if (song.getArtist() == artist) {
                count++;
            }
        }

        return count;
    }

    public String getMeanPlaylistDurationAsString() {
        Duration meanDuration = this.getMeanPlaylistDuration();
        return Util.durationToString(meanDuration);
    }

    public Duration getMeanPlaylistDuration() {
        Duration fullDuration = this.getFullPlaylistDuration();
        return fullDuration.dividedBy(this.getNumberOfSongs());
    }

    public int getNumberOfSongs() {
        return this.songs.size();
    }

    public String getFullPlaylistDurationAsString() {
        Duration fullDuration = this.getFullPlaylistDuration();
        return Util.durationToString(fullDuration);
    }

    public Duration getFullPlaylistDuration() {
        Duration fullDuration = Duration.ZERO;
        for (Song song: this.songs) {
            fullDuration = fullDuration.plus(song.getDuration());
        }

        return fullDuration;
    }

    public Song getLongestSong() {
        if (this.isEmpty()) {
            System.out.println("Error: trying to get longest song from empty playlist: " + this.getName());
            return null;
        }

        Song longest = this.songs.first();
        for (Song song: this.songs) {
            if (song.getDuration().compareTo(longest.getDuration()) > 0) {
                longest = song;
            }
        }

        return longest;
    }

    public Song getShortestSong() {
        if (this.isEmpty()) {
            System.out.println("Error: trying to get shortest song from empty playlist: " + this.getName());
            return null;
        }

        Song shortest = this.songs.first();
        for (Song song: this.songs) {
            if (song.getDuration().compareTo(shortest.getDuration()) < 0) {
                shortest = song;
            }
        }

        return shortest;
    }

    public boolean isEmpty() {
        return this.getSongs().isEmpty();
    }

    public void addSong(Song song) {
        if (this.getSongs().size() < this.getPlaylistMaxSize()) {
            this.songs.add(song);
        } else {
            System.out.println("Error: trying to add song '" + song.getName() + "' to full playlist '" + this.getName() + "'");
        }
    }

    public void removeSongByName(String songName) {
        Song toRemove = null;
        for (Song song: this.getSongs()) {
            if (song.getName() == songName) {
                toRemove = song;
                break;
            }
        }

        // Change lastPlayedSong in the case it will be removed
        // If it's the first element, lastPlayedSong receives null
        if(toRemove == this.lastPlayedSong) {
            this.lastPlayedSong = this.getSongs().lower(this.lastPlayedSong);
        }

        if (toRemove != null) {
            this.songs.remove(toRemove);
        } else {
            System.out.println("Error: no song named '" + songName + "' in playlist '" + this.getName() + "'");
        }
    }

    public TreeSet<Song> getSongs() {
        return this.songs;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public User getPlaylistOwner() {
        return this.user;
    }

    public int getPlaylistMaxSize() {
        int maxSize;
        switch (this.user.getSubscription()) {
            case PREMIUM:
                maxSize = 100;
                break;
            default:
                maxSize = 10;
                break;
        }

        return maxSize;
    }
}
